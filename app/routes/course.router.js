const express = require("express");

const courseMiddleware = require("../middlewares/course.middleware");
const courseController = require("../controllers/course.controller");

const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL course: ", req.url);

    next();
});

router.get("/", courseMiddleware.getAllCourseMiddleware, courseController.getAllCourses)

router.post("/", courseMiddleware.createCourseMiddleware, courseController.createCourse);

router.get("/:courseId", courseMiddleware.getDetailCourseMiddleware, courseController.getCourseById);

router.put("/:courseId", courseMiddleware.updateCourseMiddleware, courseController.updateCourse);

router.delete("/:courseId", courseMiddleware.deleteCourseMiddleware, courseController.deleteCourse);

module.exports = router;